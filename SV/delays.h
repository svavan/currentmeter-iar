#include "stm8s.h"
#include "stm8s_gpio.h"
#include "stm8s_clk.h"
#include "stm8s_tim4.h"

//#define LED_PORT GPIOB
//#define LED GPIO_PIN_5

#define TIM4_PERIOD       124
#define TIM4_US_PERIOD       4

volatile uint16_t count;

INTERRUPT_HANDLER(IRQ_Handler_TIM4, 23)
{
    if (count)
        count--;

    TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
}

void timerStart_ms(){
  
   TIM4_Cmd(DISABLE);       // stop
   TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
   TIM4_ClearFlag(TIM4_FLAG_UPDATE);
   TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);

   count = 0xFFFF;

   TIM4_Cmd(ENABLE);       // let's go 
}

uint16_t timerGetVal_ms(){
   uint16_t now = count;
   //TIM4_Cmd(DISABLE);       // stop
   return (0xFFFF-now);
}


uint16_t timerStop_ms(){
   uint16_t now = count;
   TIM4_Cmd(DISABLE);       // stop
   return (0xFFFF-now);
}

void delay_us(uint16_t us)
{
        TIM4_Cmd(DISABLE);       // stop
        TIM4_TimeBaseInit(TIM4_PRESCALER_4, TIM4_US_PERIOD);
        TIM4_ClearFlag(TIM4_FLAG_UPDATE);
        TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);

        count = us>>1;

        TIM4_Cmd(ENABLE);       // let's go

        while(count);
}


void delay_ms(uint16_t ms)
{
        TIM4_Cmd(DISABLE);       // stop
        TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
        TIM4_ClearFlag(TIM4_FLAG_UPDATE);
        TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);

        count = ms;

        TIM4_Cmd(ENABLE);       // let's go

    while(count);
}


void delay_us_check(uint16_t time)
{
    while (time)
    {
        delay_us(100);
        time--;
    }
}

ErrorStatus status = FALSE;

/*int main( void )
{
    // ----------- GPIO CONFIG -------------------
    GPIO_DeInit(LED_PORT);
    GPIO_Init(LED_PORT, LED, GPIO_MODE_OUT_PP_LOW_FAST);

    // ---------- CLK CONFIG -----------------------
    CLK_DeInit();

    CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
    CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1); // set 16 MHz for CPU

    // uncomment if use HSE on Quartz
    //status = CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSE, DISABLE,
    //                CLK_CURRENTCLOCKSTATE_DISABLE);

    // ---------- TIM4 CONFIG -----------------------
    TIM4_DeInit();

    enableInterrupts();

    for(;;)
    {
        delay_us_check(1000);
        GPIO_WriteReverse(LED_PORT, LED);
        delay_ms(1000);
        GPIO_WriteReverse(LED_PORT, LED);
    }
}*/