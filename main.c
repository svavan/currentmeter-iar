#include "stm8s.h"
#include "main.h"
#include "SV\delays.h"

#define LED_GPIO_PORT  GPIOA
#define LED_GPIO_PINS  GPIO_PIN_2

extern volatile uint16_t adc1;
extern __IO uint8_t Slave_Buffer_Rx[10];
//uint8_t ar[600];
char buffForUint16[10];
static uint32_t y = 0;
__IO uint32_t LsiFreq = 0;

uint16_t current,previousCurrent;

//==============================================================================
char * uint16ToAsciiHex(uint16_t num, char ** buff)
{
  char * buff1 = (char *)buff;
  buff1[4] = 0x00;
  buff1[3] = (char)(num&0x000f);
  buff1[2] = (char)((num&0x00f0)>>4);
  buff1[1] = (char)((num&0x0f00)>>8);
  buff1[0] = (char)((num&0xf000)>>12);
  
  for(uint8_t i = 0;i<4;i++)
    if(buff1[i]>9)
      buff1[i]=buff1[i]+0x37;
    else
      buff1[i]=buff1[i]+0x30;        
  return(buff1);
}
//==============================================================================
char * uint16ToAscii(uint16_t num, char ** buff)
{
  char * buff1 = (char *)buff;  
  uint32_t num1 = num;
  buff1[5] = 0x00;  
  buff1[4] = (char)(num1 % 10); //num1 = buff1[4];
  buff1[3] = (char)((num1 % 100)/10);
  buff1[2] = (char)((num1 % 1000)/100);
  buff1[1] = (char)((num1 % 10000)/1000);
  buff1[0] = (char)((num1 % 100000)/10000);
  
  for(uint8_t i = 0;i<5;i++)
      buff1[i]=buff1[i]+0x30;        
  return(buff1);
}
//==============================================================================
char * uint8ToAscii(uint8_t num, char ** buff)
{
  char * buff1 = (char *)buff;  
  uint16_t num1 = num;
  buff1[3] = 0x00;  
  buff1[2] = (char)(num1 % 10); 
  buff1[1] = (char)((num1 % 100)/10);
  buff1[0] = (char)((num1 % 1000)/100);
  
  for(uint8_t i = 0;i<3;i++)
      buff1[i]=buff1[i]+0x30;        
  return(buff1);
}
//==============================================================================
void UARTPrintf(char *message)
{
  char *ch = message;
  while (*ch)
  {
    UART1_SendData8((unsigned char) *ch);
    while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET); //  Wait for transmission to complete.
    ch++;                                     
  }
}
//==============================================================================
/*uint16_t getCurrentAC() { // ======== ONLY FOR TEST!!! SVAVAN ==================
  
#define ADC_COUNT           1023
#define ADC_COUNT_HALF      ADC_COUNT / 2
#define ADC_COUNT_ACS712_1A 37.88
#define RMS                 0.707
#define SAMPLE_TIMES        32
#define FAULT_ADC 0.011
    
  int sensorValue = 0;
    int maxValue = 0;
    int minValue = ADC_COUNT;
    float sensorCurrent = 0;
    float sensorCurrentRMS = 0;
    
    timerStart_ms();
    
    while (timerGetVal_ms() < 60) {      
      adc1=0x0000;
      ADC1_StartConversion();
      while(adc1==0x0000);
      sensorValue = adc1;
      
      if (sensorValue > maxValue) {
            maxValue = sensorValue;
        } else if (sensorValue < minValue) {
            minValue = sensorValue;
        }
    }
    
    timerStop_ms();
       
    maxValue -= maxValue * FAULT_ADC;
    minValue += minValue * FAULT_ADC;
    sensorCurrent = (((maxValue) - minValue) / 2.0) / ADC_COUNT_ACS712_1A;
    sensorCurrentRMS = sensorCurrent * RMS;
    return (uint16_t)(sensorCurrentRMS*1000);
}*/
//==============================================================================
uint16_t getAdcOneVal()
{
  adc1=0x0000;
  ADC1_StartConversion();
  while(adc1==0x0000); 
  return (adc1);
}

//==============================================================================
uint16_t getAdc1Val(uint16_t timeout)
{
      uint32_t adc1AvgValue =0;
      uint16_t countIteration = 0;
      timerStart_ms();
      while(timerGetVal_ms()<timeout)
  {
      adc1=0x0000;
      ADC1_StartConversion();
      while(adc1==0x0000);
      //return (adc1);
      adc1AvgValue += adc1;
      countIteration++;      
  }
  timerStop_ms();  
  return ((uint16_t)(adc1AvgValue/countIteration));
}

//==============================================================================
/*void printArrAdcVal()
{
  for(uint16_t i = 0; i < 600; i++ )
  {
    uint16ToAscii(ar[i]+400,(char **)(&buffForUint16));
    UARTPrintf(buffForUint16);
    UARTPrintf("\n");
    delay_ms(5);
  }
}*/
//==============================================================================
uint16_t getAdc1NoiseAvgVal(uint16_t timeOfFix,uint16_t pause, uint16_t countIteration)
{
      uint32_t adcDeltaValue =0;
      uint32_t adcDeltaValueMax =0;
      uint16_t zeroPoint = 0x201;
      //uint16_t countIteration = 0;
      
      for(uint16_t i = 0; i<countIteration; i++)
      {
      
        //adc1AvgValue += getAdc1Val(timeOfFix);
        adcDeltaValue = abs(getAdc1Val(timeOfFix)-zeroPoint);
        if(adcDeltaValue > adcDeltaValueMax)
          adcDeltaValueMax = adcDeltaValue;
        delay_ms(pause);
        
      }
  
  return ((uint16_t)(adcDeltaValueMax));
}

//==============================================================================
//==============================================================================
uint16_t getAdcValWithFilter(uint16_t x){
  //static uint16_t y = 0;
  return y = (y*31+x) / 32;
};
//==============================================================================
/*void getArrAdcVal()
{
  
  //uint16_t x = getAdc1Val(20);
  
  //getAdcValWithFilter(512);
  
  for(uint16_t i = 0; i < 128; i++ )
  {
    getAdcValWithFilter(getAdcOneVal());
  }
  
   for(uint16_t i = 0; i < 600; i++ )
  {
    ar[i] = getAdcValWithFilter(getAdcOneVal())-400;
    //ar[i] = getAdcOneVal()-400;
  }
}
*/
//==============================================================================
//==============================================================================
/*uint16_t getCurrentWithFilter1()
{
  uint32_t adc1AvgValue =0;
  uint16_t countIteration = 3682;//4200 = 7 * 600; max 8300 if curentMul <=1000
  uint16_t zeroPoint = 0x201;//0x202.8 [0x20A : 0x1fB]
  uint16_t curentMul = 411;//378
  uint16_t I = 0;
  uint16_t adcVal = 0;
  
    for(uint16_t i = 0; i < 128; i++ )
  {
    getAdcValWithFilter(getAdcOneVal());
  }
  
  //timerStart_ms();
  
  for(uint16_t i = 0; i<countIteration; i++)
  {   
    
      adc1=0x0000;
      ADC1_StartConversion();
      while(adc1==0x0000);
      adcVal = getAdcValWithFilter(adc1);
      if(adcVal>zeroPoint)
      {
        adc1AvgValue += (adcVal - zeroPoint);
      }
      else
      {  
        adc1AvgValue += (zeroPoint - adcVal );
      }      
  };
  
  //return (timerStop_ms());
  
  adc1AvgValue = adc1AvgValue * curentMul;
  I = adc1AvgValue  / countIteration / 10;
  
  //*((uint16_t *)(Slave_Buffer_Rx))=I;
  //Slave_Buffer_Rx[2] = Slave_Buffer_Rx[1] ^ Slave_Buffer_Rx[0];
  
  return (I);
}
*/
//==============================================================================
//==============================================================================
uint16_t getCurrent()
{
  uint32_t adc1AvgValue =0;
  uint16_t countIteration = 4200;//4200 = 7 * 600; max 8300 if curentMul <=1000
  //uint16_t iteration = 0;
  uint16_t zeroPoint = 0x201;//0x202.8 [0x20A : 0x1fB]
  //uint16_t zeroPointPlus = 0x201;//0x202.8 [0x20A : 0x1fB]
  //uint16_t zeroPointMinus = 0x201;
  //uint16_t finalCorection = 3;
  uint16_t curentMul = 411;//378
  uint16_t curent = 0;
  
  //timerStart_ms();
  
  for(uint16_t i = 0; i<countIteration; i++)
  {
      adc1=0x0000;
      ADC1_StartConversion();
      while(adc1==0x0000);
    
      //if(adc1>max) max = adc1;
      
      if(adc1>zeroPoint)
      {
        adc1AvgValue += (adc1 - zeroPoint);
      }
      else
      {  
        adc1AvgValue += (zeroPoint - adc1 );
      }
      
  };
  
  //return (timerStop_ms());
  
  adc1AvgValue = adc1AvgValue * curentMul;
  curent = adc1AvgValue  / countIteration / 10;
  
  //*((uint16_t *)(Slave_Buffer_Rx))=curent;
  //Slave_Buffer_Rx[2] = Slave_Buffer_Rx[1] ^ Slave_Buffer_Rx[0];
  
  return (curent);
}

//==============================================================================
uint16_t getCurrentWithNoiseFilter()
{
  uint32_t adc1AvgValue =0;
  uint16_t countIteration = 1800;//4200 = 7 * 600; max 8300 if curentMul <=1000
  uint16_t zeroPoint = 0x202;//0x202.8 [0x20A : 0x1fB]
  uint16_t curentMul = 390;//390
  uint16_t curent = 0;
  uint16_t noise = 2;
  
  //timerStart_ms();
  
  if(getAdc1NoiseAvgVal(3,4,4)/*7,5,3*/ > noise) 
      for(uint16_t i = 0; i<countIteration; i++)
      {
          adc1=0x0000;
          ADC1_StartConversion();
          while(adc1==0x0000);
          if(adc1>zeroPoint)
          {
            adc1AvgValue += (adc1 - zeroPoint);
          }
          else
          {  
            adc1AvgValue += (zeroPoint - adc1 );
          }
      };
  
  //return (timerStop_ms());
  
  //uint16_t curent1;
  
  //curent1 = (adc1AvgValue * curentMul)/ countIteration / 10; 
  
  adc1AvgValue = adc1AvgValue * curentMul;
  curent = adc1AvgValue  / countIteration / 10;
  
  //*((uint16_t *)(Slave_Buffer_Rx))=curent;
  //Slave_Buffer_Rx[2] = Slave_Buffer_Rx[1] ^ Slave_Buffer_Rx[0];
  
  return (curent);
}
//==============================================================================

//--------------------------------------------------------------------------------
static void IWDG_Config(void)
{
  /* Enable IWDG (the LSI oscillator will be enabled by hardware) */
  IWDG_Enable();
  
  /* IWDG timeout equal to 250 ms (the timeout may varies due to LSI frequency
     dispersion) */
  /* Enable write access to IWDG_PR and IWDG_RLR registers */
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
  
  /* IWDG counter clock: LSI/128 */
  IWDG_SetPrescaler(IWDG_Prescaler_128);
  
  /* Set counter reload value to obtain 250ms IWDG Timeout.
    Counter Reload Value = 250ms/IWDG counter clock period
                         = 250ms / (LSI/128)
                         = 0.25s / (LsiFreq/128)
                         = LsiFreq/(128 * 4)
                         = LsiFreq/512
   */
  IWDG_SetReload((uint8_t)(LsiFreq/512));
  
  /* Reload IWDG counter */
  IWDG_ReloadCounter();
}
//------------------------------------------------------------------------------
static uint32_t LSIMeasurment(void)
{
  uint32_t lsi_freq_hz = 0x0;
  uint32_t fmaster = 0x0;
  uint16_t ICValue1 = 0x0;
  uint16_t ICValue2 = 0x0;

  /* Get master frequency */
  fmaster = CLK_GetClockFreq();

  /* Enable the LSI measurement: LSI clock connected to timer Input Capture 1 */
  AWU->CSR |= AWU_CSR_MSR;

#if defined (STM8S903) || defined (STM8S103) || defined (STM8S003)
  /* Measure the LSI frequency with TIMER Input Capture 1 */
  
  /* Capture only every 8 events!!! */
  /* Enable capture of TI1 */
  TIM1_ICInit(TIM1_CHANNEL_1, TIM1_ICPOLARITY_RISING, TIM1_ICSELECTION_DIRECTTI,
              TIM1_ICPSC_DIV8, 0);
  
  /* Enable TIM1 */
  TIM1_Cmd(ENABLE);
  
  /* wait a capture on cc1 */
  while((TIM1->SR1 & TIM1_FLAG_CC1) != TIM1_FLAG_CC1);
  /* Get CCR1 value*/
  ICValue1 = TIM1_GetCapture1();
  TIM1_ClearFlag(TIM1_FLAG_CC1);
  
  /* wait a capture on cc1 */
  while((TIM1->SR1 & TIM1_FLAG_CC1) != TIM1_FLAG_CC1);
  /* Get CCR1 value*/
  ICValue2 = TIM1_GetCapture1();
  TIM1_ClearFlag(TIM1_FLAG_CC1);
  
  /* Disable IC1 input capture */
  TIM1->CCER1 &= (uint8_t)(~TIM1_CCER1_CC1E);
  /* Disable timer2 */
  TIM1_Cmd(DISABLE);
  
#else  
  /* Measure the LSI frequency with TIMER Input Capture 1 */
  
  /* Capture only every 8 events!!! */
  /* Enable capture of TI1 */
  TIM3_ICInit(TIM3_CHANNEL_1, TIM3_ICPOLARITY_RISING, TIM3_ICSELECTION_DIRECTTI,
              TIM3_ICPSC_DIV8, 0);

  /* Enable TIM3 */
  TIM3_Cmd(ENABLE);

  /* wait a capture on cc1 */
  while ((TIM3->SR1 & TIM3_FLAG_CC1) != TIM3_FLAG_CC1);
  /* Get CCR1 value*/
  ICValue1 = TIM3_GetCapture1();
  TIM3_ClearFlag(TIM3_FLAG_CC1);

  /* wait a capture on cc1 */
  while ((TIM3->SR1 & TIM3_FLAG_CC1) != TIM3_FLAG_CC1);
    /* Get CCR1 value*/
  ICValue2 = TIM3_GetCapture1();
  TIM3_ClearFlag(TIM3_FLAG_CC1);

  /* Disable IC1 input capture */
  TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC1E);
  /* Disable timer3 */
  TIM3_Cmd(DISABLE);
#endif

  /* Compute LSI clock frequency */
  lsi_freq_hz = (8 * fmaster) / (ICValue2 - ICValue1);
  
  /* Disable the LSI measurement: LSI clock disconnected from timer Input Capture 1 */
  AWU->CSR &= (uint8_t)(~AWU_CSR_MSR);

 return (lsi_freq_hz);
}

//--------------------------------------------------------------------------------
void adc1MyReInit(){
    GPIO_Init(GPIOC,GPIO_PIN_4,GPIO_MODE_IN_FL_NO_IT);
    ADC1_DeInit();
    ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, 
              ADC1_CHANNEL_2, 
              ADC1_PRESSEL_FCPU_D8,
              ADC1_EXTTRIG_TIM,
              DISABLE, 
              ADC1_ALIGN_RIGHT,  
              ADC1_SCHMITTTRIG_CHANNEL2, 
              DISABLE);
    ADC1_ITConfig(ADC1_IT_EOCIE ,ENABLE);
}
//--------------------------------------------------------------------------------

void main(void)
{
     // for delays
    CLK_DeInit();
    CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
    CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1); // set 16 MHz for CPU
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
    TIM4_DeInit();
	
  I2C_DeInit();

  /* Initialize I2C peripheral */
  #ifdef I2C_slave_7Bits_Address
    I2C_Init(100000, SLAVE_ADDRESS, I2C_DUTYCYCLE_2, I2C_ACK_CURR, I2C_ADDMODE_7BIT, 16);           
  #else
    I2C_Init(100000, SLAVE_ADDRESS, I2C_DUTYCYCLE_2, I2C_ACK_CURR,I2C_ADDMODE_10BIT, 16);
  #endif

  /* Enable Error Interrupt*/
  I2C_ITConfig((I2C_IT_TypeDef)(I2C_IT_ERR | I2C_IT_EVT | I2C_IT_BUF), ENABLE);


  enableInterrupts(); 
  
  GPIO_Init(LED_GPIO_PORT, (GPIO_Pin_TypeDef)LED_GPIO_PINS, GPIO_MODE_OUT_PP_LOW_FAST);
  

  GPIO_Init(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_FAST);


  
// ------------ ADC1 -------------------
  
     adc1MyReInit();
    //GPIO_Init(GPIOC,GPIO_PIN_4,GPIO_MODE_IN_FL_NO_IT);
    //ADC1_DeInit();
    //ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_2, ADC1_PRESSEL_FCPU_D8,
    //ADC1_EXTTRIG_TIM,DISABLE, ADC1_ALIGN_RIGHT,  ADC1_SCHMITTTRIG_CHANNEL2, DISABLE);
    //ADC1_ITConfig(ADC1_IT_EOCIE ,ENABLE);

// ------------ UART1 -------------------
    UART1_DeInit();
    UART1_Init((uint32_t)115200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO,
              UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);


    LsiFreq = LSIMeasurment(); 
    IWDG_Config();
    
    //adc1=0;
    enableInterrupts();
    //uint16_t current, previousCurrent;
    uint16_t countEqualVal = 0;
    
    GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_7); //On ACS712
    
    
        for(uint16_t i = 0; i < 128; i++ )
        {
          getAdcValWithFilter(getAdcOneVal());
          IWDG_ReloadCounter();
        }
    
        
    for(;;)
    {
      
        GPIO_WriteReverse(LED_GPIO_PORT, (GPIO_Pin_TypeDef)LED_GPIO_PINS);
      
        GPIO_WriteHigh(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_7); //On ACS712
        
        //curent = getAdc1Val(1);
        //curent = getAdcOneVal();
        
        //curent = getCurrentWithFilter1();
        
        current = getCurrentWithNoiseFilter();
        
        /*
        if(current!=0)
        {
        if(previousCurrent == current)
          if(countEqualVal!=0xFFFF)
          countEqualVal ++;
        
        if(previousCurrent != current)
          countEqualVal = 0;
          //if(countEqualVal!=0)
          //countEqualVal --;
        }
        
        if(countEqualVal > 20)
        {
          countEqualVal = 0;
          delay_ms(500);
        }
          
        previousCurrent = current;
        */
        
        current = getAdcValWithFilter(current);
        
        
        
        GPIO_WriteLow(GPIOC, (GPIO_Pin_TypeDef)GPIO_PIN_7); //Off ACS712
        
        //delay_ms(500);
        
        
        *((uint16_t *)(Slave_Buffer_Rx))=current;
        Slave_Buffer_Rx[2] = Slave_Buffer_Rx[1] ^ Slave_Buffer_Rx[0];
        
        uint16ToAscii(current,(char **)(&buffForUint16));
        UARTPrintf(buffForUint16);
        UARTPrintf("\n");
         
      
        //getArrAdcVal();
        //printArrAdcVal();
        //delay_ms(5);
        
        

        //ADC1_StartConversion();
        //getCurrent1();
        //delay_ms(300);
        //curent = getCurrentAC();
        //uint16ToAscii(curent,(char **)(&buffForUint16));
        //UARTPrintf(buffForUint16);
        //UARTPrintf(" | ");
        
        //curent = getCurrentWithNoiseFilter();
        //curent = getAdc1Val(1);
        //curent =getAdc1AvgVal(7,5,3);
        
       // if(curent>550){
        
        //uint16ToAscii(curent,(char **)(&buffForUint16));
        //UARTPrintf(buffForUint16);
        
         /* for(uint16_t i = 0; i<600; i++)
        {
          UARTPrintf(uint8ToAscii(ar[i],(char **)(&buffForUint16)));
          UARTPrintf("\n");
          delay_ms(20);
        }*/
        
        //UARTPrintf(" | 0x");
        
        //uint16ToAscii(curent,(char **)(&buffForUint16));
        //UARTPrintf(buffForUint16);
        //UARTPrintf("\n");
        //}
        
        
        
        
        IWDG_ReloadCounter(); 
    }
    
    
  
}


//======================================================================================
#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}
#endif

